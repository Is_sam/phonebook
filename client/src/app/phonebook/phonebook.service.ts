import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PhoneBookService {
  public API = '//localhost:8080/phonebook';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(this.API);
  }

  get(id: string) {
    return this.http.get(this.API + '/' + id);
  }

  save(person: any): Observable<any> {
    let result: Observable<Object>;
    if (person['href']) {
      result = this.http.put(person.href, person);
    } else {
      result = this.http.post(this.API, person);
    }
    return result;
  }

  remove(href: string) {
    return this.http.delete(href);
  }
}
