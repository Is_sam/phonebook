import { Component, OnInit } from '@angular/core';
import { PhoneBookService } from './phonebook.service';

@Component({
  selector: 'app-phonebook',
  templateUrl: './phonebook.component.html',
  styleUrls: ['./phonebook.component.css']
})
export class PhonebookComponent implements OnInit {
  persons: Array<any>;

  constructor(private phoneBookService: PhoneBookService) { }

  ngOnInit() {
    this.phoneBookService.getAll().subscribe(data => {
      this.persons = data;
    });
  }

}
