# Phonebook APIs

Due to a lack of time, only the endpoints and a basic interface have been created. There are some issues related to the unit tests and the authentification part has not been added.

## Requirement
<p>Your task is to build a RESTful API for a phone book application. Your APIs need to support an authentication method in order to secure your requests.  Requirements - mandatory endpoints: create, read, update, delete (feel free to extend these endpoints) - use an authentication method to secure your requests (Examples: JWT token, oAuth, etc.) - use a way to make your data persistent (database is preferred) - write at least one unit test and a functional test  We strongly recommend you use frameworks to solve the challenge if you added frameworks to your hackajob profile. Try to use good practices for the application's architecture. Extra points are given for correct use of design patterns and programming principles.  Please describe in a README.md file how the application works and write any other additional info needed about your endpoints.</p>


##  Getting Started

All Maven plugins and dependencies are available from [Maven Central](https://search.maven.org/). Please have installed
* JDK 1.8 (tested with both Oracle)
* Maven 3.5.0+
* Angular CLI: 1.6.8
* Node: 6.10.2

### Building

```
mvn clean install
```

### Running

First launuch the server

```
./mvnw spring-boot:run
```

It shall automatically run the server on the port 8080.

Then go to the client folder and launch

```
ng serve
```

### Test the Endpoint

You can tests the endpoints either by using the <strong>curl</strong> command or by running the unit tests.

```
curl -v 'http://localhost:8080/api/phonebook/'
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /api/phonebook/ HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> 
< HTTP/1.1 200 
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Thu, 15 Feb 2018 13:04:37 GMT
< 
* Connection #0 to host localhost left intact
[{"id":1,"firstname":"Luke","lastname":"Skywalker","phoneNumber":"+44120487493"},{"id":2,"firstname":"Master","lastname":"Yoda","phoneNumber":"+44120447493"},{"id":3,"firstname":"Obiwan","lastname":"Kenobi","phoneNumber":"+44122487493"},{"id":4,"firstname":"Padmé","lastname":"Amidala","phoneNumber":"+44126487493"}]% 
```

#### Links

* https://developer.okta.com/blog/2017/12/04/basic-crud-angular-and-spring-boot
* https://github.com/jhipster/jhipster-sample-app
* http://www.baeldung.com/
* https://projects.spring.io/spring-boot/
* https://cli.angular.io/