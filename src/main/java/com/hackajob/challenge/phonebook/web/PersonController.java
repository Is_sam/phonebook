package com.hackajob.challenge.phonebook.web;

import com.hackajob.challenge.phonebook.domain.entities.Person;
import com.hackajob.challenge.phonebook.domain.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by issamhammi on 13/02/2018.
 */
@RestController("/api/phonebook")
@CrossOrigin(origins = "http://localhost:4200")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    public void setPersonRepository(PersonRepository personRepository){
        this.personRepository = personRepository;
    }

    /**
     * Retrieve all persons from the phonebook
     * @return Collection of persons
     */
    @GetMapping
    public Collection<Person> getPersons() {
        return personRepository.findAll().stream()
                .collect(Collectors.toList());
    }

    /**
     * return the person from the phonebook
     * @param id of the person to be retrieved
     * @return the person or not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getPerson(@PathVariable Long id) {
        Person person = personRepository.findOne(id);
        Optional<Person> result = Optional.ofNullable(person);
        if (result.isPresent())
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    /**
     * Add a person to the phonebook
     * @param person to be added
     * @return the person newly created
     * @throws Exception in case an id has been found
     */
    @PostMapping
    public ResponseEntity<Person> createPerson(@Valid @RequestBody Person person) throws Exception {
        if (person.getId() != null) {
            throw new Exception("A new person cannot already have an ID");
        }
        Person result = personRepository.save(person);
        return new ResponseEntity<>(person, HttpStatus.CREATED);
    }

    /**
     * Edit an existing person (or add it if doesn't not exist)
     * @param person to be updated
     * @return the person updated
     * @throws Exception from the creation function
     */
    @PutMapping
    public ResponseEntity<Person> updatePerson(@Valid @RequestBody Person person) throws Exception{
        if (person.getId() == null) {
            return createPerson(person);
        }
        Person result = personRepository.save(person);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    /**
     * Delete a person from the notebook
     * @param id of the person
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable Long id) {
        personRepository.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
