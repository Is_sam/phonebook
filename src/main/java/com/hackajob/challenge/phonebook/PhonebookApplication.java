package com.hackajob.challenge.phonebook;

import com.hackajob.challenge.phonebook.domain.entities.Person;

import com.hackajob.challenge.phonebook.domain.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class PhonebookApplication {

	@Autowired
	private PersonRepository personRepository;

	public final static List<Person> persons =
			Arrays.asList(
					new Person("Luke", "Skywalker", "+44120487493"),
					new Person("Master", "Yoda", "+44120447493"),
					new Person("Obiwan", "Kenobi", "+44122487493"),
					new Person("Padmé","Amidala", "+44126487493")
			);


	public static void main(String[] args) {
		SpringApplication.run(PhonebookApplication.class, args);
	}

	@Bean
	ApplicationRunner init() {
		return args -> {
			persons.forEach(person -> {
				personRepository.save(person);
			});
		};
	}
}
