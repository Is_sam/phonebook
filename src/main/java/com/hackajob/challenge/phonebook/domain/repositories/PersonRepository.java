package com.hackajob.challenge.phonebook.domain.repositories;

import com.hackajob.challenge.phonebook.domain.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by issamhammi on 13/02/2018.
 */
@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface PersonRepository extends JpaRepository<Person, Long> {

}
