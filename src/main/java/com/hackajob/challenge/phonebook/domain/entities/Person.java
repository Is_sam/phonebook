package com.hackajob.challenge.phonebook.domain.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by issamhammi on 13/02/2018.
 * A person is represented by a firstname, a lastname and a phone number.
 */
@Entity
@Getter @Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    private @NonNull String firstname;
    private @NonNull String lastname;
    private @NonNull String phoneNumber;

    public Person(String firstname, String lastname, String phoneNumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoneNumber = phoneNumber;
    }
}
